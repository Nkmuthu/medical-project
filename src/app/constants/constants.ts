export const shortMonths =
    ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

export const chartConfigs = {
    targetColor: '#fbc733',
    statusColor: '#d79229',
    dataLabelColor: '#fff'
};

export const durationDropdown = [
    { 'label': 'Weekly', value: 'weekly' },
    { 'label': 'Monthly', value: 'monthly' },
    { 'label': 'Quarterly', value: 'quarterly' },
    { 'label': 'Yearly', value: 'yearly' },
];
