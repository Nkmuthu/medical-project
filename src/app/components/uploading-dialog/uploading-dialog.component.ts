import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-uploading-dialog',
  templateUrl: './uploading-dialog.component.html',
  styleUrls: ['./uploading-dialog.component.scss']
})
export class UploadingDialogComponent implements OnInit {
  @Input() visible = false;
  @Output() onHide: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
