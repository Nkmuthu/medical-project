import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadingDialogComponent } from './uploading-dialog.component';

describe('UploadingDialogComponent', () => {
  let component: UploadingDialogComponent;
  let fixture: ComponentFixture<UploadingDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadingDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
