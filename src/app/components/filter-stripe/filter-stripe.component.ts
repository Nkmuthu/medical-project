import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SharedEmiterService } from '../../modules/shared/shared-emiter.service';
@Component({
  selector: 'app-filter-stripe',
  templateUrl: './filter-stripe.component.html',
  styleUrls: ['./filter-stripe.component.scss']
})
export class FilterStripeComponent implements OnInit {
  nameSortOrder: any;
  lastVistSortOrder: any;
  constructor(
    private sharedEmiter: SharedEmiterService
  ) { }

  ngOnInit() {
  }

  sortByName() {
    this.lastVistSortOrder = undefined;
    this.nameSortOrder = (this.nameSortOrder === 1) ? -1 : 1;
    this.sharedEmiter.nameShort.emit(this.nameSortOrder);
  }

  sortByLastVist() {
    this.nameSortOrder = undefined;
    this.lastVistSortOrder = (this.lastVistSortOrder === 1) ? -1 : 1;
    this.sharedEmiter.lastVisitShort.emit(this.lastVistSortOrder);
  }

}
