import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterStripeComponent } from './filter-stripe.component';

describe('FilterStripeComponent', () => {
  let component: FilterStripeComponent;
  let fixture: ComponentFixture<FilterStripeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterStripeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterStripeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
