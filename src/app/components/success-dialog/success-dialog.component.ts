import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-success-dialog',
  templateUrl: './success-dialog.component.html',
  styleUrls: ['./success-dialog.component.scss']
})
export class SuccessDialogComponent implements OnInit {
  @Input() visible = false;
  @Output() onHide: EventEmitter<any> = new EventEmitter();
  constructor(
    private location: Location
  ) { }

  ngOnInit() {
  }

}
