import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() burgerIcon = true;
  @Input() arrowLeftIcon = false;
  @Input() search = true;
  @Input() header: String;
  @Input() fromDate: String;
  @Input() endDate: String;
  @Input() lastUpdated: String;
  routerOptions: any = { exact: false };
  viewId: any;
  type: any;

  constructor(
    private location: Location,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.viewId = params['viewId'];
      this.type = params['type'];
    });
  }

}
