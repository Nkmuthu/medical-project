import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-choose-file-dialog',
  templateUrl: './choose-file-dialog.component.html',
  styleUrls: ['./choose-file-dialog.component.scss']
})
export class ChooseFileDialogComponent implements OnInit {
  @Input() visible = false;
  @Input() viewId: any;
  @Output() onHide: EventEmitter<any> = new EventEmitter();
  @Output() uploadHandler: EventEmitter<any> = new EventEmitter();

  constructor(
  ) { }

  ngOnInit() {
  }

  clearFiles(inputFile) {
    inputFile.basicFileInput.nativeElement.value = null;
    inputFile.clear();
  }

  onUpload(event, inputFile) {
    this.clearFiles(inputFile);
    this.uploadHandler.emit(event);
  }

}
