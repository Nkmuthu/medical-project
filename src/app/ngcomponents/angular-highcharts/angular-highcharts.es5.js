import * as Highcharts from 'highcharts';
import { Directive, ElementRef, Input, Inject, Injectable, InjectionToken, NgModule } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var Chart = /** @class */ (function () {
    function Chart(options) {
        if (options === void 0) { options = { series: [] }; }
        // init series array if not set
        if (!options.series) {
            options.series = [];
        }
        this.options = options;
    }
    Object.defineProperty(Chart.prototype, "options", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.ref) {
                return this.ref.options;
            }
            return this._options;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._options = value;
            if (this.ref) {
                this.ref.update(value);
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Add Point
     * \@memberof Chart
     * @param {?} point         Highcharts.DataPoint, number touple or number
     * @param {?=} serieIndex    Index position of series. This defaults to 0.
     * @param {?=} redraw        Flag whether or not to redraw point. This defaults to true.
     * @param {?=} shift         Shift point to the start of series. This defaults to false.
     * @return {?}
     */
    Chart.prototype.addPoint = /**
     * Add Point
     * \@memberof Chart
     * @param {?} point         Highcharts.DataPoint, number touple or number
     * @param {?=} serieIndex    Index position of series. This defaults to 0.
     * @param {?=} redraw        Flag whether or not to redraw point. This defaults to true.
     * @param {?=} shift         Shift point to the start of series. This defaults to false.
     * @return {?}
     */
    function (point, serieIndex, redraw, shift) {
        if (serieIndex === void 0) { serieIndex = 0; }
        if (redraw === void 0) { redraw = true; }
        if (shift === void 0) { shift = false; }
        if (this.ref && this.ref.series.length > serieIndex) {
            this.ref.series[serieIndex].addPoint(point, redraw, shift);
            return;
        }
        // keep options in snyc if chart is not initialized
        if (this.options.series.length > serieIndex) {
            this.options.series[serieIndex].data.push(point);
        }
    };
    /**
     * Add Series
     * \@memberof Chart
     * @param {?} serie         Series Configuration
     * @param {?=} redraw        Flag whether or not to redraw series. This defaults to true.
     * @param {?=} animation     Whether to apply animation, and optionally animation configuration. This defaults to false.
     * @return {?}
     */
    Chart.prototype.addSerie = /**
     * Add Series
     * \@memberof Chart
     * @param {?} serie         Series Configuration
     * @param {?=} redraw        Flag whether or not to redraw series. This defaults to true.
     * @param {?=} animation     Whether to apply animation, and optionally animation configuration. This defaults to false.
     * @return {?}
     */
    function (serie, redraw, animation) {
        if (redraw === void 0) { redraw = true; }
        if (animation === void 0) { animation = false; }
        if (this.ref) {
            this.ref.addSeries(serie, redraw, animation);
            return;
        }
        // keep options in snyc if chart is not initialized
        this.options.series.push(serie);
    };
    /**
     * Remove Point
     * \@memberof Chart
     * @param {?} pointIndex    Index of Point
     * @param {?=} serieIndex    Specified Index of Series. Defaults to 0.
     * @return {?}
     */
    Chart.prototype.removePoint = /**
     * Remove Point
     * \@memberof Chart
     * @param {?} pointIndex    Index of Point
     * @param {?=} serieIndex    Specified Index of Series. Defaults to 0.
     * @return {?}
     */
    function (pointIndex, serieIndex) {
        if (serieIndex === void 0) { serieIndex = 0; }
        if (this.ref &&
            this.ref.series.length > serieIndex &&
            this.ref.series[serieIndex].data.length > pointIndex) {
            this.ref.series[serieIndex].removePoint(pointIndex, true);
            return;
        }
        // keep options in snyc if chart is not initialized
        if (this.options.series.length > serieIndex &&
            this.options.series[serieIndex].data.length > pointIndex) {
            this.options.series[serieIndex].data.splice(pointIndex, 1);
        }
    };
    /**
     * Remove Series
     * \@memberof Chart
     * @param {?} serieIndex    Index position of series to remove.
     * @return {?}
     */
    Chart.prototype.removeSerie = /**
     * Remove Series
     * \@memberof Chart
     * @param {?} serieIndex    Index position of series to remove.
     * @return {?}
     */
    function (serieIndex) {
        if (this.ref && this.ref.series.length > serieIndex) {
            this.ref.series[serieIndex].remove(true);
            return;
        }
        // keep options in snyc if chart is not initialized
        if (this.options.series.length > serieIndex) {
            this.options.series.splice(serieIndex, 1);
        }
    };
    return Chart;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var MapChart = /** @class */ (function () {
    function MapChart(options) {
        this.options = options;
    }
    return MapChart;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var StockChart = /** @class */ (function () {
    function StockChart(options) {
        if (options === void 0) { options = { series: [] }; }
        this.options = options;
    }
    return StockChart;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var ChartDirective = /** @class */ (function () {
    function ChartDirective(el) {
        this.el = el;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ChartDirective.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (!changes["chart"].isFirstChange()) {
            this.destroy();
            this.init();
        }
    };
    /**
     * @return {?}
     */
    ChartDirective.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.init();
    };
    /**
     * @return {?}
     */
    ChartDirective.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.destroy(true);
    };
    /**
     * @return {?}
     */
    ChartDirective.prototype.init = /**
     * @return {?}
     */
    function () {
        if (this.chart instanceof Chart) {
            return this.chart.ref = Highcharts.chart(this.el.nativeElement, this.chart.options);
        }
        if (this.chart instanceof StockChart) {
            return this.chart.ref = (/** @type {?} */ (Highcharts)).stockChart(this.el.nativeElement, this.chart.options);
        }
        if (this.chart instanceof MapChart) {
            return this.chart.ref = (/** @type {?} */ (Highcharts)).mapChart(this.el.nativeElement, this.chart.options);
        }
    };
    /**
     * @param {?=} sync
     * @return {?}
     */
    ChartDirective.prototype.destroy = /**
     * @param {?=} sync
     * @return {?}
     */
    function (sync) {
        if (sync === void 0) { sync = false; }
        if (this.chart && this.chart.ref) {
            if (sync) {
                this.chart.options = this.chart.ref.options;
            }
            this.chart.ref.destroy();
            delete this.chart.ref;
        }
    };
    ChartDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[chart]'
                },] },
    ];
    /** @nocollapse */
    ChartDirective.ctorParameters = function () { return [
        { type: ElementRef, },
    ]; };
    ChartDirective.propDecorators = {
        "chart": [{ type: Input },],
    };
    return ChartDirective;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var /** @type {?} */ HIGHCHARTS_MODULES = new InjectionToken('HighchartsModules');
var ChartService = /** @class */ (function () {
    function ChartService(chartModules) {
        this.chartModules = chartModules;
    }
    /**
     * @return {?}
     */
    ChartService.prototype.initModules = /**
     * @return {?}
     */
    function () {
        this.chartModules.forEach(function (chartModule) {
            chartModule(Highcharts);
        });
    };
    ChartService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    ChartService.ctorParameters = function () { return [
        { type: Array, decorators: [{ type: Inject, args: [HIGHCHARTS_MODULES,] },] },
    ]; };
    return ChartService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var ɵ0 = [];
var ChartModule = /** @class */ (function () {
    function ChartModule(cs) {
        this.cs = cs;
        this.cs.initModules();
    }
    ChartModule.decorators = [
        { type: NgModule, args: [{
                    exports: [ChartDirective],
                    declarations: [ChartDirective],
                    providers: [
                        { provide: HIGHCHARTS_MODULES, useValue: ɵ0 },
                        ChartService
                    ]
                },] },
    ];
    /** @nocollapse */
    ChartModule.ctorParameters = function () { return [
        { type: ChartService, },
    ]; };
    return ChartModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { Highcharts, ChartModule, HIGHCHARTS_MODULES, Chart, StockChart, MapChart, ChartDirective as ɵb, ChartService as ɵa };
