/// <reference path="../../types/highcharts/highstock.d.ts" />
export declare class StockChart {
    ref: Highstock.ChartObject;
    options: Highstock.Options;
    constructor(options?: Highstock.Options);
}
