import * as Highcharts from 'highcharts';
import { Directive, ElementRef, Input, Inject, Injectable, InjectionToken, NgModule } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class Chart {
    /**
     * @param {?} value
     * @return {?}
     */
    set options(value) {
        this._options = value;
        if (this.ref) {
            this.ref.update(value);
        }
    }
    /**
     * @return {?}
     */
    get options() {
        if (this.ref) {
            return this.ref.options;
        }
        return this._options;
    }
    /**
     * @param {?=} options
     */
    constructor(options = { series: [] }) {
        // init series array if not set
        if (!options.series) {
            options.series = [];
        }
        this.options = options;
    }
    /**
     * Add Point
     * \@memberof Chart
     * @param {?} point         Highcharts.DataPoint, number touple or number
     * @param {?=} serieIndex    Index position of series. This defaults to 0.
     * @param {?=} redraw        Flag whether or not to redraw point. This defaults to true.
     * @param {?=} shift         Shift point to the start of series. This defaults to false.
     * @return {?}
     */
    addPoint(point, serieIndex = 0, redraw = true, shift = false) {
        if (this.ref && this.ref.series.length > serieIndex) {
            this.ref.series[serieIndex].addPoint(point, redraw, shift);
            return;
        }
        // keep options in snyc if chart is not initialized
        if (this.options.series.length > serieIndex) {
            this.options.series[serieIndex].data.push(point);
        }
    }
    /**
     * Add Series
     * \@memberof Chart
     * @param {?} serie         Series Configuration
     * @param {?=} redraw        Flag whether or not to redraw series. This defaults to true.
     * @param {?=} animation     Whether to apply animation, and optionally animation configuration. This defaults to false.
     * @return {?}
     */
    addSerie(serie, redraw = true, animation = false) {
        if (this.ref) {
            this.ref.addSeries(serie, redraw, animation);
            return;
        }
        // keep options in snyc if chart is not initialized
        this.options.series.push(serie);
    }
    /**
     * Remove Point
     * \@memberof Chart
     * @param {?} pointIndex    Index of Point
     * @param {?=} serieIndex    Specified Index of Series. Defaults to 0.
     * @return {?}
     */
    removePoint(pointIndex, serieIndex = 0) {
        if (this.ref &&
            this.ref.series.length > serieIndex &&
            this.ref.series[serieIndex].data.length > pointIndex) {
            this.ref.series[serieIndex].removePoint(pointIndex, true);
            return;
        }
        // keep options in snyc if chart is not initialized
        if (this.options.series.length > serieIndex &&
            this.options.series[serieIndex].data.length > pointIndex) {
            this.options.series[serieIndex].data.splice(pointIndex, 1);
        }
    }
    /**
     * Remove Series
     * \@memberof Chart
     * @param {?} serieIndex    Index position of series to remove.
     * @return {?}
     */
    removeSerie(serieIndex) {
        if (this.ref && this.ref.series.length > serieIndex) {
            this.ref.series[serieIndex].remove(true);
            return;
        }
        // keep options in snyc if chart is not initialized
        if (this.options.series.length > serieIndex) {
            this.options.series.splice(serieIndex, 1);
        }
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MapChart {
    /**
     * @param {?} options
     */
    constructor(options) {
        this.options = options;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class StockChart {
    /**
     * @param {?=} options
     */
    constructor(options = { series: [] }) {
        this.options = options;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ChartDirective {
    /**
     * @param {?} el
     */
    constructor(el) {
        this.el = el;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (!changes["chart"].isFirstChange()) {
            this.destroy();
            this.init();
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.init();
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.destroy(true);
    }
    /**
     * @return {?}
     */
    init() {
        if (this.chart instanceof Chart) {
            return this.chart.ref = Highcharts.chart(this.el.nativeElement, this.chart.options);
        }
        if (this.chart instanceof StockChart) {
            return this.chart.ref = (/** @type {?} */ (Highcharts)).stockChart(this.el.nativeElement, this.chart.options);
        }
        if (this.chart instanceof MapChart) {
            return this.chart.ref = (/** @type {?} */ (Highcharts)).mapChart(this.el.nativeElement, this.chart.options);
        }
    }
    /**
     * @param {?=} sync
     * @return {?}
     */
    destroy(sync = false) {
        if (this.chart && this.chart.ref) {
            if (sync) {
                this.chart.options = this.chart.ref.options;
            }
            this.chart.ref.destroy();
            delete this.chart.ref;
        }
    }
}
ChartDirective.decorators = [
    { type: Directive, args: [{
                selector: '[chart]'
            },] },
];
/** @nocollapse */
ChartDirective.ctorParameters = () => [
    { type: ElementRef, },
];
ChartDirective.propDecorators = {
    "chart": [{ type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
let /** @type {?} */ HIGHCHARTS_MODULES = new InjectionToken('HighchartsModules');
class ChartService {
    /**
     * @param {?} chartModules
     */
    constructor(chartModules) {
        this.chartModules = chartModules;
    }
    /**
     * @return {?}
     */
    initModules() {
        this.chartModules.forEach(chartModule => {
            chartModule(Highcharts);
        });
    }
}
ChartService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ChartService.ctorParameters = () => [
    { type: Array, decorators: [{ type: Inject, args: [HIGHCHARTS_MODULES,] },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const ɵ0 = [];
class ChartModule {
    /**
     * @param {?} cs
     */
    constructor(cs) {
        this.cs = cs;
        this.cs.initModules();
    }
}
ChartModule.decorators = [
    { type: NgModule, args: [{
                exports: [ChartDirective],
                declarations: [ChartDirective],
                providers: [
                    { provide: HIGHCHARTS_MODULES, useValue: ɵ0 },
                    ChartService
                ]
            },] },
];
/** @nocollapse */
ChartModule.ctorParameters = () => [
    { type: ChartService, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { Highcharts, ChartModule, HIGHCHARTS_MODULES, Chart, StockChart, MapChart, ChartDirective as ɵb, ChartService as ɵa };
