/// <reference path="../../types/highcharts/index.d.ts" />
/**
 * @license
 * Copyright Felix Itzenplitz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at
 * https://github.com/cebor/angular-highcharts/blob/master/LICENSE
 * @author Felix Itzenplitz
 * @author Timothy A. Perez (contributor)
 */
export declare type Point = number | [number, number] | Highcharts.DataPoint;
export declare class Chart {
    private _options;
    ref: Highcharts.ChartObject;
    options: Highcharts.Options;
    constructor(options?: Highcharts.Options);
    /**
     * Add Point
     * @param point         Highcharts.DataPoint, number touple or number
     * @param serieIndex    Index position of series. This defaults to 0.
     * @param redraw        Flag whether or not to redraw point. This defaults to true.
     * @param shift         Shift point to the start of series. This defaults to false.
     * @memberof Chart
     */
    addPoint(point: Point, serieIndex?: number, redraw?: boolean, shift?: boolean): void;
    /**
     * Add Series
     * @param serie         Series Configuration
     * @param redraw        Flag whether or not to redraw series. This defaults to true.
     * @param animation     Whether to apply animation, and optionally animation configuration. This defaults to false.
     * @memberof Chart
     */
    addSerie(serie: Highcharts.SeriesOptions, redraw?: boolean, animation?: boolean | Highcharts.Animation): void;
    /**
     * Remove Point
     * @param pointIndex    Index of Point
     * @param serieIndex    Specified Index of Series. Defaults to 0.
     * @memberof Chart
     */
    removePoint(pointIndex: number, serieIndex?: number): void;
    /**
     * Remove Series
     * @param serieIndex    Index position of series to remove.
     * @memberof Chart
     */
    removeSerie(serieIndex: number): void;
}
