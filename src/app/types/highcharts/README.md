# Installation
> `npm install --save @types/highcharts`

# Summary
This package contains type definitions for Highcharts (http://www.highcharts.com/).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/highcharts

Additional Details
 * Last updated: Thu, 15 Mar 2018 23:17:55 GMT
 * Dependencies: geojson
 * Global values: Highcharts, HighchartsBoost, HighchartsExporting, HighchartsMore, HighchartsNoDataToDisplay, HighchartsOfflineExporting, Highstock

# Credits
These definitions were written by Damiano Gambarotto <https://github.com/damianog>, Dan Lewi Harkestad <https://github.com/baltie>, Albert Ozimek <https://github.com/AlbertOzimek>, Juliën Hanssens <https://github.com/hanssens>.
