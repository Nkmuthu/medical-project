import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: '', redirectTo: '/MR/U1/dashboard', pathMatch: 'full' },
  {
    path: ':type/:viewId/dashboard',
    loadChildren: 'app/modules/dashboard/dashboard.module#DashboardModule',
  },
  {
    path: ':type/:viewId/doctors',
    loadChildren: 'app/modules/doctors/doctors.module#DoctorsModule',
  },
  {
    path: ':type/:viewId/chemists',
    loadChildren: 'app/modules/chemists/chemists.module#ChemistsModule',
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { useHash: true })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
