import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { ChemistsComponent } from './chemists.component';
import { ChemistsListViewComponent } from './chemists-list-view/chemists-list-view.component';
import { ChemistsMapViewComponent } from './chemists-map-view/chemists-map-view.component';
import { SingleChemistComponent } from './single-chemist/single-chemist.component';
import { SingleChemistDetailsComponent } from './single-chemist/single-chemist-details/single-chemist-details.component';
import { SingleChemistDataComponent } from './single-chemist/single-chemist-data/single-chemist-data.component';

import { ChemistsService } from './services/chemists.service';
import { ChemistsEmiterService } from './services/chemists-emiter.service';


const routes: Routes = [
  {
    path: '', component: ChemistsComponent,
    children: [
      { path: '', component: ChemistsListViewComponent },
      { path: 'map-view', component: ChemistsMapViewComponent },
    ]
  },
  {
    path: ':chemistId', component: SingleChemistComponent,
    children: [
      { path: '', component: SingleChemistDetailsComponent },
      { path: 'data', component: SingleChemistDataComponent },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ChemistsComponent,
    ChemistsListViewComponent,
    ChemistsMapViewComponent,
    SingleChemistComponent,
    SingleChemistDetailsComponent,
    SingleChemistDataComponent
  ],
  providers: [
    ChemistsService,
    ChemistsEmiterService
  ],
  exports: [
    RouterModule
  ]
})
export class ChemistsModule { }
