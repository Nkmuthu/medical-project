import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChemistsListViewComponent } from './chemists-list-view.component';

describe('ChemistsListViewComponent', () => {
  let component: ChemistsListViewComponent;
  let fixture: ComponentFixture<ChemistsListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChemistsListViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChemistsListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
