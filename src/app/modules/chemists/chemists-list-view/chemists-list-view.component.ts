import { Component, OnInit } from '@angular/core';
import { ChemistsService } from '../services/chemists.service';
import { environment } from '../../../../environments/environment';
import { SharedEmiterService } from '../../../modules/shared/shared-emiter.service';
@Component({
  selector: 'app-chemists-list-view',
  templateUrl: './chemists-list-view.component.html',
  styleUrls: ['./chemists-list-view.component.scss']
})
export class ChemistsListViewComponent implements OnInit {

  baseUrl = environment.baseUrl;
  chemists: any[] = [];

  constructor(
    private csService: ChemistsService,
    private sharedEmiter: SharedEmiterService
  ) { }

  ngOnInit() {
    this.sharedEmiter.nameShort.subscribe(sortOrder => {
      this.sortArray(this.chemists, 'name', sortOrder);
    });
    this.sharedEmiter.lastVisitShort.subscribe(sortOrder => {
      this.sortArray(this.chemists, 'lastVisit', sortOrder);
    });
    this.csService.getAllChemists().subscribe(res => {
      this.chemists = res.data;
      console.log('this.chemists', this.chemists);
    }, err => {
      console.log('Error while calling getAllChemists');
    });
  }

  sortArray(data, field, sortOrder) {
    data.sort((data1, data2) => {
      const value1 = data1[field];
      const value2 = data2[field];
      let result = null;
      if (value1 == null && value2 != null) {
        result = -1;
      } else if (value1 != null && value2 == null) {
        result = 1;
      } else if (value1 == null && value2 == null) {
        result = 0;
      } else if (typeof value1 === 'string' && typeof value2 === 'string') {
        result = value1.localeCompare(value2);
      } else {
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
      }
      return (sortOrder * result);
    });
  }

}
