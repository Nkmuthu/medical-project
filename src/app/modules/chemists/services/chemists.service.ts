import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { environment } from '../../../../environments/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class ChemistsService {
  apiUrl = `${environment.baseUrl}${environment.version}`;
  constructor(private _http: Http) {

  }

  getAllChemists() {
    return this._http.get(`${this.apiUrl}/chemists`).map(res => res.json());
  }

  getAllProducts() {
    return this._http.get(`${this.apiUrl}/products`).map(res => res.json());
  }

  getChemist(chemistId) {
    return this._http.get(`${this.apiUrl}/chemists/${chemistId}`).map(res => res.json());
  }

  addChemistProducts(payload) {
    return this._http.post(`${this.apiUrl}/product-sales`, payload).map(res => res.json());
  }
}
