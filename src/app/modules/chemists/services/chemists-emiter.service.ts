import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class ChemistsEmiterService {
  public singleChemist: EventEmitter<any> = new EventEmitter();
  constructor() { }

}
