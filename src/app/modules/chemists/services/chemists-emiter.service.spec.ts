import { TestBed, inject } from '@angular/core/testing';

import { ChemistsEmiterService } from './chemists-emiter.service';

describe('ChemistsEmiterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChemistsEmiterService]
    });
  });

  it('should be created', inject([ChemistsEmiterService], (service: ChemistsEmiterService) => {
    expect(service).toBeTruthy();
  }));
});
