import { TestBed, inject } from '@angular/core/testing';

import { ChemistsService } from './chemists.service';

describe('ChemistsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChemistsService]
    });
  });

  it('should be created', inject([ChemistsService], (service: ChemistsService) => {
    expect(service).toBeTruthy();
  }));
});
