import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ChemistsEmiterService } from '../services/chemists-emiter.service';
@Component({
  selector: 'app-single-chemist',
  templateUrl: './single-chemist.component.html',
  styleUrls: ['./single-chemist.component.scss']
})
export class SingleChemistComponent implements OnInit {
  windowHeight: any;
  viewId: any;
  chemistId: any;
  tabs = [];
  chemist: any = {};
  constructor(
    private cmsEmitService: ChemistsEmiterService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.windowHeight = window.innerHeight;
    this.cmsEmitService.singleChemist.subscribe((chemist) => {
      this.chemist = chemist;
    });
    this.route.params.subscribe(params => {
      this.viewId = params['viewId'];
      const type = params['type'];
      this.chemistId = params['chemistId'];
      this.tabs = [
        { 'text': 'DETAILS', 'url': `/${type}/${this.viewId}/chemists/${this.chemistId}` },
        { 'text': 'DATA', 'url': `/${type}/${this.viewId}/chemists/${this.chemistId}/data` },
      ];
    });
  }

}
