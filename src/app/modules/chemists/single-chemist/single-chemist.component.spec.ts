import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleChemistComponent } from './single-chemist.component';

describe('SingleChemistComponent', () => {
  let component: SingleChemistComponent;
  let fixture: ComponentFixture<SingleChemistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleChemistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleChemistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
