import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleChemistDataComponent } from './single-chemist-data.component';

describe('SingleChemistDataComponent', () => {
  let component: SingleChemistDataComponent;
  let fixture: ComponentFixture<SingleChemistDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleChemistDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleChemistDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
