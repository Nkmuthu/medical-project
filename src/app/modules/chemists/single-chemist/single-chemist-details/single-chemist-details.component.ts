import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ChemistsService } from '../../services/chemists.service';
import { ChemistsEmiterService } from '../../services/chemists-emiter.service';
import { environment } from '../../../../../environments/environment';
declare var FileReader: any;
import { PapaParseService } from 'ngx-papaparse';
import { SharedService } from '../../../shared/shared.service';
import * as moment from 'moment';

@Component({
  selector: 'app-single-chemist-details',
  templateUrl: './single-chemist-details.component.html',
  styleUrls: ['./single-chemist-details.component.scss']
})
export class SingleChemistDetailsComponent implements OnInit {
  baseUrl = environment.baseUrl;
  chemist: any;
  windowHeight: any;
  tabs: any[];
  chemistId: any;
  chooseFileVisble = false;
  csvData: any[] = [];
  viewId: any;
  successDialogVisble = false;
  uploadingDialogVisble = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private cmsService: ChemistsService,
    private csvParseService: PapaParseService,
    private sharedService: SharedService,
    private cmsEmitServise: ChemistsEmiterService,
  ) {

  }

  ngOnInit() {
    this.windowHeight = window.innerHeight;
    this.route.params.subscribe(params => {
      this.viewId = params['viewId'];
      const type = params['type'];
      this.chemistId = params['chemistId'];
      this.tabs = [
        { 'text': 'DETAILS', 'url': `/${type}/${this.viewId}/chemists/${this.chemistId}` },
        { 'text': 'DATA', 'url': `/${type}/${this.viewId}/chemists/${this.chemistId}/data` },
      ];
      this.cmsService.getChemist(this.chemistId).subscribe(res => {
        this.chemist = (res.data) ? res.data : {};
        this.cmsEmitServise.singleChemist.emit(this.chemist);
        console.log('this.chemist', this.chemist);
      }, err => {
        console.log('Error while calling getChemist');
      });
    });
  }

  onUploadCsv(event) {
    const files = event.files;
    this.chooseFileVisble = false;
    const reader = new FileReader();
    this.csvData = [];
    reader.onload = (e) => {
      const csvData: string = reader.result;
      this.csvParseService.parse(csvData, {
        complete: (results, file) => {
          console.log('results.data', results.data);
          results.data.map((data: any, index) => {
            if (data.length > 0 && data[0] !== '' && data[1] !== '' && data[2] !== '' && data[3] !== '' && index > 0) {
              const sDate = data[results.data[0].indexOf('Date Of Sold')];
              const soldDate = moment(sDate, 'DD/MM/YYYY').utc(sDate).format();
              this.csvData.push({
                'chemistId': this.chemistId,
                'doctorLicenseNo': data[results.data[0].indexOf('Doctor License No')],
                'productId': data[results.data[0].indexOf('Product ID')],
                'quantity': data[results.data[0].indexOf('Quantity')],
                'dateOfSold': soldDate,
                'userId': this.viewId
              });
            }
          });
        }
      });
    };
    reader.onloadend = () => {
      console.log(this.csvData);
      this.cmsService.getAllProducts().subscribe(res => {
        const filteredProducts = this.csvData.filter((product: any) => {
          const prodIndex = res.data.findIndex((pro: any) => {
            return pro.productId === product.productId;
          });
          return prodIndex >= 0;
        });
        const payload = {
          'payload': filteredProducts
        };
        console.log('payload', payload);
        if (filteredProducts.length > 0) {
          this.uploadingDialogVisble = true;
          this.cmsService.addChemistProducts(payload).subscribe(res1 => {
            if (res1.success && res1.message.includes('Successfully')) {
              this.uploadingDialogVisble = false;
              this.successDialogVisble = true;
            }
          }, err => {
            console.log('Error while calling addChemistProducts');
          });
        }
      }, err => {
        console.log('Error while calling getAllProducts');
      });
    };
    reader.readAsBinaryString(files[0]);
  }
}
