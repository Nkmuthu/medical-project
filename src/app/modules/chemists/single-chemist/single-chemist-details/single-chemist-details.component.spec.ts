import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleChemistDetailsComponent } from './single-chemist-details.component';

describe('SingleChemistDetailsComponent', () => {
  let component: SingleChemistDetailsComponent;
  let fixture: ComponentFixture<SingleChemistDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleChemistDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleChemistDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
