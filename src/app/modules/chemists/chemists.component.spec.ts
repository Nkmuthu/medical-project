import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChemistsComponent } from './chemists.component';

describe('ChemistsComponent', () => {
  let component: ChemistsComponent;
  let fixture: ComponentFixture<ChemistsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChemistsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChemistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
