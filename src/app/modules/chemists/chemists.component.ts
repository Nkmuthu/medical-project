import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-chemists',
  templateUrl: './chemists.component.html',
  styleUrls: ['./chemists.component.scss']
})
export class ChemistsComponent implements OnInit {
  tabs: any;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      const type = params['type'];
      const viewId = params['viewId'];
      this.tabs = [
        { 'text': 'LIST VIEW', 'url': `/${type}/${viewId}/chemists`, 'icon': 'app-list-view' },
        { 'text': 'MAP VIEW', 'url': `/${type}/${viewId}/chemists/map-view`, 'icon': 'app-map-view' },
      ];
    });
  }

}
