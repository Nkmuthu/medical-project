import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChemistsMapViewComponent } from './chemists-map-view.component';

describe('ChemistsMapViewComponent', () => {
  let component: ChemistsMapViewComponent;
  let fixture: ComponentFixture<ChemistsMapViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChemistsMapViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChemistsMapViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
