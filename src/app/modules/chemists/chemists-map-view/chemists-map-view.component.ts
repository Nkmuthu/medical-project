import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chemists-map-view',
  templateUrl: './chemists-map-view.component.html',
  styleUrls: ['./chemists-map-view.component.scss']
})
export class ChemistsMapViewComponent implements OnInit {

  mapWidth: any;
  mapHeight: any;
  constructor() { }

  ngOnInit() {
    this.mapWidth = window.innerWidth;
    this.mapHeight = window.innerHeight;
  }

}
