import { Component, OnInit } from '@angular/core';
import { Chart } from '../../ngcomponents/angular-highcharts';
import { Router, ActivatedRoute } from '@angular/router';
import { DashboardService } from './services/dashboard.service';
import * as moment from 'moment';
import { durationDropdown, chartConfigs } from '../../constants/constants';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  windowHeight: any;
  charOptions = {
    chart: {
      type: 'bar'
    },
    title: {
      text: ``,
      floating: true,
      align: 'left',
      useHTML: true,
      verticalAlign: 'top',
      y: 30
    },
    subtitle: {
      text: null
    },
    xAxis: {
      categories: [],
      title: {
        text: null,
      },
      labels: {
        style: {
          fontSize: '8px'
        }
      },
      lineWidth: 0.5,
      tickWidth: 0.5
    },
    yAxis: {
      title: {
        text: null,
      },
      opposite: true,
      labels: {
        enabled: true,
      },
      lineWidth: 0.5,
      gridLineWidth: 0,
      // tickInterval: 20,
      tickWidth: 0.5
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true,
          align: 'right',
          color: chartConfigs.dataLabelColor,
          shadow: false
        }
      },
      series: {
        cursor: 'pointer',
        allowPointSelect: true,
        point: {
          events: {
            click: this.onPointSelect.bind(this)
          }
        }
      }
    },
    legend: {
      layout: 'horizontal',
      align: 'right',
      verticalAlign: 'top',
      symbolHeight: 12,
      symbolWidth: 12,
      symbolRadius: 0,
    },
    credits: {
      enabled: false
    },
    series: [{
      name: 'Target',
      data: [],
      color: chartConfigs.targetColor,
    }, {
      name: 'Current Status',
      data: [],
      color: chartConfigs.statusColor,
    }]
  };
  chart = new Chart();
  viewId: any;
  durations = durationDropdown;
  selectedDuartion: any = 'weekly';
  products: any[] = [];
  selectedProducts: any[] = [];
  startOfWeek: any;
  endOfWeek: any;
  startOfMonth: any;
  endOfMonth: any;
  startOfQuarter: any;
  endOfQuarter: any;
  startOfYear: any;
  endOfYear: any;
  startDate: any;
  endDate: any;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dsService: DashboardService
  ) {
    this.startOfWeek = moment().startOf('week').utc().format();
    this.endOfWeek = moment().endOf('week').utc().format();
    this.startOfMonth = moment().startOf('month').utc().format();
    this.endOfMonth = moment().endOf('month').utc().format();
    this.startOfQuarter = moment().startOf('quarter').utc().format();
    this.endOfQuarter = moment().endOf('quarter').utc().format();
    this.startOfYear = moment().startOf('year').utc().format();
    this.endOfYear = moment().endOf('year').utc().format();
    this.startDate = this.startOfWeek;
    this.endDate = this.endOfWeek;
  }

  ngOnInit() {
    this.windowHeight = window.innerHeight;
    this.route.params.subscribe(params => {
      this.viewId = params['viewId'];
      this.dsService.getAllUserProducts(this.viewId).subscribe(res => {
        console.log('getAllUserProducts', res);
        if (res.success) {
          this.products = res.data;
          this.selectedProducts = res.data;
          this.getProductTargetSales();
        }
      }, err => {
        console.log('Error While Calling getAllUserProducts', err);
      });
    });
  }

  goToPreviousDuration() {
    switch (this.selectedDuartion) {
      case 'monthly': {
        this.startDate = moment(this.startDate).subtract(1, 'M').utc().format();
        this.endDate = moment(this.startDate).endOf('month').utc().format();
        break;
      }
      case 'quarterly': {
        this.startDate = moment(this.startDate).subtract(1, 'Q').utc().format();
        this.endDate = moment(this.startDate).endOf('quarter').utc().format();
        break;
      }
      case 'yearly': {
        this.startDate = moment(this.startDate).subtract(1, 'y').utc().format();
        this.endDate = moment(this.startDate).endOf('year').utc().format();
        break;
      }
      default: {
        this.startDate = moment(this.startDate).subtract(1, 'w').utc().format();
        this.endDate = moment(this.endDate).subtract(1, 'w').utc().format();
        break;
      }
    }
    this.getProductTargetSales();
  }

  goToNextDuration() {
    switch (this.selectedDuartion) {
      case 'monthly': {
        this.startDate = moment(this.startDate).add(1, 'M').utc().format();
        this.endDate = moment(this.startDate).endOf('month').utc().format();
        break;
      }
      case 'quarterly': {
        this.startDate = moment(this.startDate).add(1, 'Q').utc().format();
        this.endDate = moment(this.startDate).endOf('quarter').utc().format();
        break;
      }
      case 'yearly': {
        this.startDate = moment(this.startDate).add(1, 'y').utc().format();
        this.endDate = moment(this.startDate).endOf('year').utc().format();
        break;
      }
      default: {
        this.startDate = moment(this.startDate).add(1, 'w').utc().format();
        this.endDate = moment(this.endDate).add(1, 'w').utc().format();
        break;
      }
    }
    this.getProductTargetSales();
  }

  onChangeDurationType() {
    switch (this.selectedDuartion) {
      case 'monthly': {
        this.startDate = this.startOfMonth;
        this.endDate = this.endOfMonth;
        break;
      }
      case 'quarterly': {
        this.startDate = this.startOfQuarter;
        this.endDate = this.endOfQuarter;
        break;
      }
      case 'yearly': {
        this.startDate = this.startOfYear;
        this.endDate = this.endOfYear;
        break;
      }
      default: {
        this.startDate = this.startOfWeek;
        this.endDate = this.endOfWeek;
        break;
      }
    }
    this.getProductTargetSales();
  }

  getProductTargetSales() {
    if (this.viewId && this.selectedDuartion && this.startDate && this.endDate && this.selectedProducts) {
      this.dsService.getProductTargetSales(
        this.viewId, this.selectedDuartion, this.startDate, this.endDate, this.selectedProducts
      ).subscribe(res => {
        console.log('getProductTargetSales', res);
        if (res.success) {
          this.charOptions.xAxis.categories = res.data.xAxis;
          this.charOptions.series[0].data = res.data.target;
          this.charOptions.series[1].data = res.data.acheived;
          // const start = moment(this.startDate).format('MMM-DD YY');
          // const end = moment(this.endDate).format('MMM-DD YY');
          // this.charOptions.title.text
          //   = `<span class='chart-tile-text'>${start} - ${end}</span>`;
          this.chart = new Chart(this.charOptions);
        }
      }, err => {
        console.log('Error While Calling getProductTargetSales', err);
      });
    }
  }

  onPointSelect(event) {
    console.log(event.point.category);
    const productName = event.point.category;
    const index = this.products.findIndex(product => product.name === productName);
    const productId = this.products[index].productId;
    this.router.navigate([`${productName}/${productId}`], {
      relativeTo: this.route
    });
  }

}
