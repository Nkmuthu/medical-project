import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'app-product-week-sales-chemist',
  templateUrl: './product-week-sales-chemist.component.html',
  styleUrls: ['./product-week-sales-chemist.component.scss']
})
export class ProductWeekSalesChemistComponent implements OnInit {
  chemistSales: any[] = [];
  productId: any;
  viewId: any;
  startDate: any;
  endDate: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dsService: DashboardService
  ) { }

  ngOnInit() {
    this.route.parent.params.subscribe(params => {
      this.viewId = params['viewId'];
      this.productId = params['prodId'];
      this.startDate = params['start'];
      this.endDate = params['end'];
      this.dsService.getProductSalesByChemist(
        this.viewId, this.productId, this.startDate, this.endDate
      ).subscribe(res => {
        if (res.success) {
          this.chemistSales = res.data;
        }
        console.log('getProductSalesByChemist', res);
      }, err => {
        console.log('Error While Calling getProductSalesByChemist', err);
      });
    });
  }

}
