import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductWeekSalesChemistComponent } from './product-week-sales-chemist.component';

describe('ProductWeekSalesChemistComponent', () => {
  let component: ProductWeekSalesChemistComponent;
  let fixture: ComponentFixture<ProductWeekSalesChemistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductWeekSalesChemistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductWeekSalesChemistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
