import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
@Component({
  selector: 'app-product-week-sales',
  templateUrl: './product-week-sales.component.html',
  styleUrls: ['./product-week-sales.component.scss']
})
export class ProductWeekSalesComponent implements OnInit {
  windowHeight: any;
  product: any;
  productId: any;
  startDate: any;
  week: any;
  endDate: any;
  startD: any;
  endD: any;
  tabs = [];
  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.windowHeight = window.innerHeight;
    this.route.params.subscribe(params => {
      console.log(params);
      this.product = params['product'];
      this.productId = params['prodId'];
      this.startDate = params['start'];
      this.endDate = params['end'];
      this.startD = moment(this.startDate).format('MMM DD');
      this.endD = moment(this.endDate).format('MMM DD');
      const type = params['type'];
      const viewId = params['viewId'];
      this.tabs = [
        {
          'text': 'DOCTOR',
          'url': `/${type}/${viewId}/dashboard/${this.product}/${this.productId}/${this.startDate}/${this.endDate}`
        },
        {
          'text': 'CHEMIST',
          'url': `/${type}/${viewId}/dashboard/${this.product}/${this.productId}/${this.startDate}/${this.endDate}/chemist`
        },
      ];
    });
  }

}
