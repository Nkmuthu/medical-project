import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductWeekSalesComponent } from './product-week-sales.component';

describe('ProductWeekSalesComponent', () => {
  let component: ProductWeekSalesComponent;
  let fixture: ComponentFixture<ProductWeekSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductWeekSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductWeekSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
