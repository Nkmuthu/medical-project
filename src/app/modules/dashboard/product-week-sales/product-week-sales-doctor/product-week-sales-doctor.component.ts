import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'app-product-week-sales-doctor',
  templateUrl: './product-week-sales-doctor.component.html',
  styleUrls: ['./product-week-sales-doctor.component.scss']
})
export class ProductWeekSalesDoctorComponent implements OnInit {
  productId: any;
  doctorSales: any[] = [];
  viewId: any;
  startDate: any;
  endDate: any;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dsService: DashboardService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.productId = params['prodId'];
      this.viewId = params['viewId'];
      this.startDate = params['start'];
      this.endDate = params['end'];
      this.dsService.getProductSalesByDoctor(
        this.viewId, this.productId, this.startDate, this.endDate
      ).subscribe(res => {
        if (res.success) {
          this.doctorSales = res.data;
        }
        console.log('getProductSalesByDoctor', res);
      }, err => {
        console.log('Error While Calling getProductSalesByDoctor', err);
      });
    });
  }

}
