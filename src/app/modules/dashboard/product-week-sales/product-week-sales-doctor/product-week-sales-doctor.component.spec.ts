import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductWeekSalesDoctorComponent } from './product-week-sales-doctor.component';

describe('ProductWeekSalesDoctorComponent', () => {
  let component: ProductWeekSalesDoctorComponent;
  let fixture: ComponentFixture<ProductWeekSalesDoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductWeekSalesDoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductWeekSalesDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
