import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { environment } from '../../../../environments/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class DashboardService {
  apiUrl = `${environment.baseUrl}${environment.version}`;
  constructor(private _http: Http) {

  }

  getAllUserProducts(userId) {
    return this._http.get(`${this.apiUrl}/user/products/${userId}`).map(res => res.json());
  }

  getProductTargetSales(userId, duration, startDate, endDate, products) {
    return this._http.post(`${this.apiUrl}/product-target-sales/${userId}/${duration}/${startDate}/${endDate}`,
      { 'payload': products }).map(res => res.json());
  }

  getSingleProductTargetSales(duration, userId, productId, startEndDates) {
    return this._http.post(`${this.apiUrl}/single-product-target-sales/${duration}/${userId}/${productId}`,
      { 'payload': startEndDates }).map(res => res.json());
  }

  getProductSalesByDoctor(userId, productId, startDate, endDate) {
    return this._http.get(`${this.apiUrl}/product-sales-by-doctor/${userId}/${productId}/${startDate}/${endDate}`).map(res => res.json());
  }

  getProductSalesByChemist(userId, productId, startDate, endDate) {
    return this._http.get(`${this.apiUrl}/product-sales-by-chemist/${userId}/${productId}/${startDate}/${endDate}`).map(res => res.json());
  }
}
