import { Component, OnInit } from '@angular/core';
import { Chart } from '../../../ngcomponents/angular-highcharts';
import { Router, ActivatedRoute } from '@angular/router';
import { DashboardService } from '../services/dashboard.service';
import * as moment from 'moment';
import { durationDropdown, chartConfigs } from '../../../constants/constants';

@Component({
  selector: 'app-single-product-target',
  templateUrl: './single-product-target.component.html',
  styleUrls: ['./single-product-target.component.scss']
})
export class SingleProductTargetComponent implements OnInit {
  windowHeight: any;
  productName: any;
  productId: any;
  chartOptions = {
    chart: {
      type: 'column'
    },
    title: {
      text: ``,
      floating: true,
      align: 'left',
      useHTML: true,
      verticalAlign: 'top',
      y: 30
    },
    xAxis: {
      categories: [],
      lineWidth: 0.5,
      tickWidth: 0.5
    },
    yAxis: [
      {
        min: 0,
        title: {
          text: null
        },
        lineWidth: 0.5,
        gridLineWidth: 0,
        // tickInterval: 20,
        tickWidth: 0.5
      }
    ],
    legend: {
      layout: 'horizontal',
      align: 'right',
      verticalAlign: 'top',
      symbolHeight: 12,
      symbolWidth: 12,
      symbolRadius: 0,
    },
    credits: {
      enabled: false
    },
    plotOptions: {
      column: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          verticalAlign: 'top',
          color: chartConfigs.dataLabelColor,
          shadow: false
        }
      },
      series: {
        cursor: 'pointer',
        allowPointSelect: true,
        point: {
          events: {
            click: this.onPointSelect.bind(this)
          }
        }
      }
    },
    series: [{
      name: 'Target',
      color: chartConfigs.targetColor,
      data: [],
      pointWidth: 30,
      pointPlacement: 0.17
    }, {
      name: 'Achieved',
      color: chartConfigs.statusColor,
      data: [],
      pointWidth: 20,
      pointPlacement: -0.13
    }]
  };
  chart = new Chart();

  durations = durationDropdown;
  selectedDuartion: any = 'weekly';
  viewId: any;
  startEndDates: any[] = [];
  startEndDatesDummy: any[] = [];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dsService: DashboardService
  ) {
    const startOfWeek = moment().startOf('week').utc().format();
    this.makeStartEndDatesArray(startOfWeek, 'week', 'w');
  }

  makeStartEndDatesArray(startDate, type, typeShort, next?: boolean) {
    const startEndDates = [];
    for (let i = 0; i < 4; i++) {
      const endDate = moment(startDate).endOf(type).utc().format();
      startEndDates.push({
        'startDate': startDate,
        'endDate': endDate
      });

      startDate = (next) ? moment(startDate).add(1, typeShort).utc().format() :
        moment(startDate).subtract(1, typeShort).utc().format();
    }
    this.startEndDates = (next) ? startEndDates : startEndDates.reverse();
  }


  ngOnInit() {
    this.windowHeight = window.innerHeight;
    this.route.params.subscribe(params => {
      this.productName = params['product'];
      this.productId = params['prodId'];
      this.viewId = params['viewId'];
      this.getSingleProductTargetSales();
    });
  }

  onChangeDurationType() {
    switch (this.selectedDuartion) {
      case 'monthly': {
        const startOfmonth = moment().startOf('month').utc().format();
        this.makeStartEndDatesArray(startOfmonth, 'month', 'M');
        break;
      }
      case 'quarterly': {
        const startOfQuarter = moment().startOf('quarter').utc().format();
        this.makeStartEndDatesArray(startOfQuarter, 'quarter', 'Q');
        break;
      }
      case 'yearly': {
        const startOfYear = moment().startOf('year').utc().format();
        this.makeStartEndDatesArray(startOfYear, 'year', 'y');
        break;
      }
      default: {
        const startOfWeek = moment().startOf('week').utc().format();
        this.makeStartEndDatesArray(startOfWeek, 'week', 'w');
        break;
      }
    }
    this.getSingleProductTargetSales();
  }

  goToPreviousDuration() {
    const currentFirstStart = this.startEndDates[0].startDate;
    switch (this.selectedDuartion) {
      case 'monthly': {
        const startDate = moment(currentFirstStart).subtract(1, 'M').utc().format();
        this.makeStartEndDatesArray(startDate, 'month', 'M');
        break;
      }
      case 'quarterly': {
        const startDate = moment(currentFirstStart).subtract(1, 'Q').utc().format();
        this.makeStartEndDatesArray(startDate, 'quarter', 'Q');
        break;
      }
      case 'yearly': {
        const startDate = moment(currentFirstStart).subtract(1, 'y').utc().format();
        this.makeStartEndDatesArray(startDate, 'year', 'y');
        break;
      }
      default: {
        const startDate = moment(currentFirstStart).subtract(1, 'w').utc().format();
        this.makeStartEndDatesArray(startDate, 'week', 'w');
        break;
      }
    }
    this.getSingleProductTargetSales();
  }

  goToNextDuration() {
    const currentLastStart = this.startEndDates[3].startDate;
    switch (this.selectedDuartion) {
      case 'monthly': {
        const startDate = moment(currentLastStart).add(1, 'M').utc().format();
        this.makeStartEndDatesArray(startDate, 'month', 'M', true);
        break;
      }
      case 'quarterly': {
        const startDate = moment(currentLastStart).add(1, 'Q').utc().format();
        this.makeStartEndDatesArray(startDate, 'quarter', 'Q', true);
        break;
      }
      case 'yearly': {
        const startDate = moment(currentLastStart).add(1, 'y').utc().format();
        this.makeStartEndDatesArray(startDate, 'year', 'y', true);
        break;
      }
      default: {
        const startDate = moment(currentLastStart).add(1, 'w').utc().format();
        this.makeStartEndDatesArray(startDate, 'week', 'w', true);
        break;
      }
    }
    this.getSingleProductTargetSales();
  }

  getSingleProductTargetSales() {
    if (this.viewId && this.productId && this.selectedDuartion && this.startEndDates.length === 4) {
      this.dsService.getSingleProductTargetSales(this.selectedDuartion, this.viewId, this.productId, this.startEndDates).subscribe(res => {
        console.log('getSingleProductTargetSales', res);
        if (res.success) {
          this.chartOptions.series[0].data = res.data.target;
          this.chartOptions.series[1].data = res.data.acheived;
          this.startEndDates = res.data.startEndDates;
          this.startEndDatesDummy = [];
          this.startEndDates.map(dates => {
            const start = moment(dates.startDate).format('MMM-DD YY');
            const end = moment(dates.endDate).format('MMM-DD YY');
            this.startEndDatesDummy.push(`${start}  -  ${end}`);
          });
          this.chartOptions.xAxis.categories = this.startEndDatesDummy;
          this.chart = new Chart(this.chartOptions);
        }
      }, err => {
        console.log('Error While Calling getSingleProductTargetSales', err);
      });
    }
  }

  onPointSelect(event) {
    const index = this.startEndDatesDummy.indexOf(event.point.category);
    const dates = this.startEndDates[index];
    this.router.navigate([`${dates.startDate}/${dates.endDate}`], {
      relativeTo: this.route
    });
  }

}
