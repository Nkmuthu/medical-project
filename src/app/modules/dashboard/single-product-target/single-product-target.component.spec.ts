import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleProductTargetComponent } from './single-product-target.component';

describe('SingleProductTargetComponent', () => {
  let component: SingleProductTargetComponent;
  let fixture: ComponentFixture<SingleProductTargetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleProductTargetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleProductTargetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
