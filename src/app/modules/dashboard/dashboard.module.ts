import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextModule } from 'primeng/inputtext';
import { DashboardComponent } from './dashboard.component';
import { SingleProductTargetComponent } from './single-product-target/single-product-target.component';
import { ProductWeekSalesComponent } from './product-week-sales/product-week-sales.component';
import { ProductWeekSalesDoctorComponent } from './product-week-sales/product-week-sales-doctor/product-week-sales-doctor.component';
import { ProductWeekSalesChemistComponent } from './product-week-sales/product-week-sales-chemist/product-week-sales-chemist.component';

import { DashboardService } from './services/dashboard.service';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: ':product/:prodId', component: SingleProductTargetComponent },
  {
    path: ':product/:prodId/:start/:end', component: ProductWeekSalesComponent,
    children: [
      { path: '', component: ProductWeekSalesDoctorComponent },
      { path: 'chemist', component: ProductWeekSalesChemistComponent },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CheckboxModule,
    InputTextModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    DashboardComponent,
    SingleProductTargetComponent,
    ProductWeekSalesComponent,
    ProductWeekSalesDoctorComponent,
    ProductWeekSalesChemistComponent
  ],
  providers: [
    DashboardService
  ],
  exports: [
    RouterModule
  ]
})
export class DashboardModule { }
