import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorsListViewComponent } from './doctors-list-view.component';

describe('DoctorsListViewComponent', () => {
  let component: DoctorsListViewComponent;
  let fixture: ComponentFixture<DoctorsListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorsListViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorsListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
