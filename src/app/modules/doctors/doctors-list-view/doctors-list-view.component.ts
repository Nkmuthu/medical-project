import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doctors-list-view',
  templateUrl: './doctors-list-view.component.html',
  styleUrls: ['./doctors-list-view.component.scss']
})
export class DoctorsListViewComponent implements OnInit {
  doctors: any[] = [
    {
      'img_url': 'assets/images/doctor.png', 'doctor_name': 'Kalimuthu', 'doctor_id': 'DR-125',
      'address': 'Kormangala, Bangalore', 'dept': 'Cardiology', 'last_view': '13-03-208'
    },
    {
      'img_url': 'assets/images/doctor.png', 'doctor_name': 'Kalimuthu', 'doctor_id': 'DR-125',
      'address': 'Kormangala, Bangalore', 'dept': 'Cardiology', 'last_view': '13-03-208'
    },
    {
      'img_url': 'assets/images/doctor.png', 'doctor_name': 'Kalimuthu', 'doctor_id': 'DR-125',
      'address': 'Kormangala, Bangalore', 'dept': 'Cardiology', 'last_view': '13-03-208'
    },
    {
      'img_url': 'assets/images/doctor.png', 'doctor_name': 'Kalimuthu', 'doctor_id': 'DR-125',
      'address': 'Kormangala, Bangalore', 'dept': 'Cardiology', 'last_view': '13-03-208'
    },
    {
      'img_url': 'assets/images/doctor.png', 'doctor_name': 'Kalimuthu', 'doctor_id': 'DR-125',
      'address': 'Kormangala, Bangalore', 'dept': 'Cardiology', 'last_view': '13-03-208'
    },
    {
      'img_url': 'assets/images/doctor.png', 'doctor_name': 'Kalimuthu', 'doctor_id': 'DR-125',
      'address': 'Kormangala, Bangalore', 'dept': 'Cardiology', 'last_view': '13-03-208'
    },
    {
      'img_url': 'assets/images/doctor.png', 'doctor_name': 'Kalimuthu', 'doctor_id': 'DR-125',
      'address': 'Kormangala, Bangalore', 'dept': 'Cardiology', 'last_view': '13-03-208'
    },
    {
      'img_url': 'assets/images/doctor.png', 'doctor_name': 'Kalimuthu', 'doctor_id': 'DR-125',
      'address': 'Kormangala, Bangalore', 'dept': 'Cardiology', 'last_view': '13-03-208'
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
