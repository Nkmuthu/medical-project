import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { DoctorsComponent } from './doctors.component';
import { DoctorsListViewComponent } from './doctors-list-view/doctors-list-view.component';
import { DoctorsMapViewComponent } from './doctors-map-view/doctors-map-view.component';

const doctorsRoutes: Routes = [
  {
    path: '', component: DoctorsComponent,
    children: [
      { path: '', component: DoctorsListViewComponent },
      { path: 'map-view', component: DoctorsMapViewComponent },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(doctorsRoutes)
  ],
  declarations: [
    DoctorsComponent,
    DoctorsListViewComponent,
    DoctorsMapViewComponent
  ],
  exports: [
    RouterModule
  ]
})
export class DoctorsModule { }
