import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doctors-map-view',
  templateUrl: './doctors-map-view.component.html',
  styleUrls: ['./doctors-map-view.component.scss']
})
export class DoctorsMapViewComponent implements OnInit {
  mapWidth: any;
  mapHeight: any;
  constructor() { }

  ngOnInit() {
    this.mapWidth = window.innerWidth;
    this.mapHeight = window.innerHeight
  }

}
