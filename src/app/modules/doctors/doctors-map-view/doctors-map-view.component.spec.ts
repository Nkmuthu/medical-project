import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorsMapViewComponent } from './doctors-map-view.component';

describe('DoctorsMapViewComponent', () => {
  let component: DoctorsMapViewComponent;
  let fixture: ComponentFixture<DoctorsMapViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorsMapViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorsMapViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
