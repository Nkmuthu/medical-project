import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.scss']
})
export class DoctorsComponent implements OnInit {
  tabs: any[] = [];
  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const type = params['type'];
      const viewId = params['viewId'];
      this.tabs = [
        { 'text': 'LIST VIEW', 'url': `/${type}/${viewId}/doctors`, 'icon': 'app-list-view' },
        { 'text': 'MAP VIEW', 'url': `/${type}/${viewId}/doctors/map-view`, 'icon': 'app-map-view' },
      ];
    });
  }

}
