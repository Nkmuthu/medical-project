import { Injectable } from '@angular/core';

@Injectable()
export class SharedService {
  
  csvData: any;
  constructor() { }

  setCsvData(data) {
    this.csvData = data;
  }

  getCsvData() {
    return this.csvData;
  }

}
