import { TestBed, inject } from '@angular/core/testing';

import { SharedEmiterService } from './shared-emiter.service';

describe('SharedEmiterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SharedEmiterService]
    });
  });

  it('should be created', inject([SharedEmiterService], (service: SharedEmiterService) => {
    expect(service).toBeTruthy();
  }));
});
