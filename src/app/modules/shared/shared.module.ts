import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { MatSidenavModule } from '@angular/material/sidenav';
import { DialogModule } from 'primeng/dialog';
import { FileUploadModule } from 'primeng/fileupload';
import { PapaParseModule } from 'ngx-papaparse';
import { CheckboxModule } from 'primeng/checkbox';
import { ProgressBarModule } from 'primeng/progressbar';
import { ChartModule } from '../../ngcomponents/angular-highcharts';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';

import { HeaderComponent } from '../../components/header/header.component';
import { TabViewComponent } from '../../components/tab-view/tab-view.component';
import { FilterStripeComponent } from '../../components/filter-stripe/filter-stripe.component';
import { FooterComponent } from '../../components/footer/footer.component';
import { ChooseFileDialogComponent } from '../../components/choose-file-dialog/choose-file-dialog.component';
import { SuccessDialogComponent } from '../../components/success-dialog/success-dialog.component';
import { UploadingDialogComponent } from '../../components/uploading-dialog/uploading-dialog.component';

import { SharedService } from './shared.service';
import { SharedEmiterService } from './shared-emiter.service';

const MODULES = [
  FormsModule,
  RouterModule,
  HttpModule,
  MatSidenavModule,
  DialogModule,
  FileUploadModule,
  PapaParseModule,
  CheckboxModule,
  ProgressBarModule,
  ChartModule,
  TableModule,
  MultiSelectModule,
  DropdownModule
];

const COMPONENTS = [
  HeaderComponent,
  TabViewComponent,
  FilterStripeComponent,
  FooterComponent,
  ChooseFileDialogComponent,
  SuccessDialogComponent,
  UploadingDialogComponent
];

const SERVICES = [
  SharedService, SharedEmiterService
];

@NgModule({
  imports: [
    CommonModule,
    ...MODULES
  ],
  declarations: [
    ...COMPONENTS
  ],
  providers: [
    ...SERVICES
  ],
  exports: [
    ...MODULES, ...COMPONENTS
  ]
})
export class SharedModule { }
