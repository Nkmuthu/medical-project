import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class SharedEmiterService {
  public nameShort: EventEmitter<any> = new EventEmitter();
  public lastVisitShort: EventEmitter<any> = new EventEmitter();
  constructor() { }

}
