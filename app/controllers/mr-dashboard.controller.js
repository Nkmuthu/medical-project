var productSales = require('../models/product-sales.model');
var targets = require('../models/targets.model');
var doctors = require('../models/doctors.model');
var chemists = require('../models/chemists.model');
var moment = require('moment');
var async = require('async');

exports.getProductTargetSales = (req, res) => {
    var userProducts = req.body.payload;
    var startDate = req.params.start;
    var endDate = req.params.end;
    var userId = req.params.userId;
    var type = req.params.type;
    var taskArray = [];
    userProducts.map(userProduct => {
        var task = (callback) => {
            async.parallel({
                target: (callback1) => {
                    targets.findOne({
                        'fromDate': { $gte: startDate, $lt: endDate },
                        'endDate': { $gte: startDate, $lt: endDate },
                        type: type,
                        userId: userId,
                        productId: userProduct.productId
                    }, (err1, target) => {
                        if (err1)
                            callback1(err1);
                        callback1(null, target);
                    });
                },
                sales: (callback1) => {
                    productSales.find({
                        dateOfSold: { $gte: startDate, $lt: endDate },
                        userId: userId,
                        productId: userProduct.productId
                    }, (err1, sales) => {
                        if (err1)
                            callback1(err1);
                        callback1(null, sales);
                    });
                }
            }, (err1, result) => {
                if (err1)
                    callback(err1);
                userProduct.target = result.target;
                userProduct.sales = result.sales;
                callback(null, userProduct);
            });
        }
        taskArray.push(task);
    });
    async.parallel(taskArray, (err1, result) => {
        if (err1)
            res.send(err1);
        var chart = {
            'xAxis': [],
            'target': [],
            'acheived': [],
        };
        result.map(data => {
            chart.xAxis.push(data.name);
            if (data.target && data.target.target) {
                chart.target.push(data.target.target);
            } else {
                chart.target.push(0);
            }
            var acheived = data.sales.reduce((sum, elem) => {
                return sum + elem.quantity;
            }, 0);
            chart.acheived.push(acheived);
        });
        res.send({ 'success': true, result, 'data': chart });
    });
}

exports.getSingleProductTargetSales = (req, res) => {
    var userId = req.params.userId;
    var productId = req.params.productId;
    var startEndDates = req.body.payload;
    var type = req.params.type;
    var taskArray = [];
    startEndDates.map(dates => {
        var task = (callback) => {
            async.parallel({
                target: (callback1) => {
                    targets.findOne({
                        'fromDate': { $gte: dates.startDate, $lt: dates.endDate },
                        'endDate': { $gte: dates.startDate, $lt: dates.endDate },
                        type: type,
                        userId: userId,
                        productId: productId
                    }, (err1, target) => {
                        if (err1)
                            callback1(err1);
                        callback1(null, target);
                    });
                },
                sales: (callback1) => {
                    productSales.find({
                        dateOfSold: { $gte: dates.startDate, $lt: dates.endDate },
                        userId: userId,
                        productId: productId
                    }, (err1, sales) => {
                        if (err1)
                            callback1(err1);
                        callback1(null, sales);
                    });
                }
            }, (err1, result) => {
                if (err1)
                    callback(err1);
                dates.target = result.target;
                dates.sales = result.sales;
                callback(null, dates);
            });
        }
        taskArray.push(task);
    });
    async.parallel(taskArray, (err1, result) => {
        if (err1)
            res.send(err1);
        var chart = {
            'startEndDates': [],
            'target': [],
            'acheived': [],
        };
        result.map(data => {
            chart.startEndDates.push({
                'startDate': data.startDate,
                'endDate': data.endDate,
            })
            if (data.target && data.target.target) {
                chart.target.push(data.target.target);
            } else {
                chart.target.push(0);
            }
            var acheived = data.sales.reduce((sum, elem) => {
                return sum + elem.quantity;
            }, 0);
            chart.acheived.push(acheived);
        })
        res.send({ 'success': true, result, 'data': chart });
    });
}

exports.getProductSalesByDoctor = (req, res) => {
    var userId = req.params.userId;
    var productId = req.params.productId;
    var startDate = req.params.startDate;
    var endDate = req.params.endDate;
    productSales.find({
        dateOfSold: { $gte: startDate, $lt: endDate },
        userId: userId,
        productId: productId
    }, (err, sales) => {
        if (err)
            res.send(err);

        var taskArray = [];
        sales.map(sale => {
            var task = (callback) => {
                doctors.findOne({ licenseNo: sale.doctorLicenseNo }, (err1, doctor) => {
                    if (err1)
                        callback(err1);

                    sale.doctor = doctor;
                    callback(null, sale);
                })
            };
            taskArray.push(task);
        });
        async.parallel(taskArray, (err2, data) => {
            if (err2)
                res.send(err2);

            res.send({ 'success': true, data });
        });
    });
}

exports.getProductSalesByChemist = (req, res) => {
    var userId = req.params.userId;
    var productId = req.params.productId;
    var startDate = req.params.startDate;
    var endDate = req.params.endDate;
    productSales.find({
        dateOfSold: { $gte: startDate, $lt: endDate },
        userId: userId,
        productId: productId
    }, (err, sales) => {
        if (err)
            res.send(err);

        var taskArray = [];
        sales.map(sale => {
            var task = (callback) => {
                chemists.findOne({ chemistId: sale.chemistId }, (err1, chemist) => {
                    if (err1)
                        callback(err1);

                    sale.chemist = chemist;
                    callback(null, sale);
                })
            };
            taskArray.push(task);
        });
        async.parallel(taskArray, (err2, data) => {
            if (err2)
                res.send(err2);

            res.send({ 'success': true, data });
        });
    });
}