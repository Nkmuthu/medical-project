var userProducts = require('../models/user-products.model');
var productsM = require('../models/products.model');
var async = require('async');

exports.getAllUserProducts = (req, res) => {
    userProducts.find((err, data) => {
        if (err)
            res.send(err);

        res.send({ 'success': true, data });
    });
}

exports.createUserProducts = (req, res) => {
    userProducts.create(req.body.payload, (err, data) => {
        if (err)
            res.send(err);

        res.send({ 'success': true, data });
    });
}

exports.getUserProductById = (req, res) => {
    userProducts.find({ userId: req.params.userId }, (err, userProducts) => {
        if (err)
            res.send(err);
        var taskArray = [];
        userProducts.map(userProduct => {
            var task = (callback) => {
                productsM.findOne({
                    productId: userProduct.productId
                }, (err1, product) => {
                    if (err1)
                        callback(err1);
                    callback(null, product);
                });
            };
            taskArray.push(task);
        });
        async.parallel(taskArray, (err1, result) => {
            res.send({ 'success': true, 'data': result });
        });
    });
}