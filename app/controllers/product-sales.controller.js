var productSales = require('../models/product-sales.model');

exports.createProductSales = (req, res) => {
    productSales.create(req.body.payload, (err, data) => {
        if (err)
            res.send(err);

        res.send({ 'success': true, 'message': 'Successfully Inserted', data });
    });
}