var users = require('../models/users.model');

exports.getAllUsers = (req, res) => {
    users.find((err, data) => {
        if (err)
            res.send(err);

        res.send({ 'success': true, data });
    });
}

exports.createUsers = (req, res) => {
    users.create(req.body.payload, (err, data) => {
        if (err)
            res.send(err);

        res.send({ 'success': true, data });
    });
}

exports.getUserById = (req, res) => {
    users.findOne({ userId: req.params.userId }, (err, data) => {
        if (err)
            res.send(err);
        res.send({ 'success': true, data });
    });
}