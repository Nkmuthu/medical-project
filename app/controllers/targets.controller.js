var targets = require('../models/targets.model');

exports.getAllTargets = (req, res) => {
    targets.find((err, data) => {
        if (err)
            res.send(err);

        res.send({ 'success': true, data });
    });
}

exports.createTargets = (req, res) => {
    targets.create(req.body.payload, (err, data) => {
        if (err)
            res.send(err);

        res.send({ 'success': true, data });
    });
}

exports.getTargetByUserId = (req, res) => {
    targets.findOne({ userId: req.params.userId }, (err, data) => {
        if (err)
            res.send(err);
        res.send({ 'success': true, data });
    });
}