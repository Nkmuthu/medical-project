var products = require('../models/products.model');

exports.getAllProducts = (req, res) => {
    products.find((err, data) => {
        if (err)
            res.send(err);

        res.send({ 'success': true, data });
    });
}

exports.createProducts = (req, res) => {
    products.create(req.body.payload, (err, data) => {
        if (err)
            res.send(err);

        res.send({ 'success': true, data });
    });
}