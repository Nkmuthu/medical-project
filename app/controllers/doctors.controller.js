var doctors = require('../models/doctors.model');

exports.getAllDoctors = (req, res) => {
    doctors.find((err, data) => {
        if (err)
            res.send(err);

        res.send({ 'success': true, data });
    });
}

exports.createDoctors = (req, res) => {
    doctors.create(req.body.payload, (err, data) => {
        if (err)
            res.send(err);

        res.send({ 'success': true, data });
    });
}

exports.getDoctorById = (req, res) => {
    doctors.findOne({ doctorId: req.params.doctorId }, (err, data) => {
        if (err)
            res.send(err);
        res.send({ 'success': true, data });
    });
}