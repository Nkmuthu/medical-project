var chemists = require('../models/chemists.model');

exports.getAllChemists = (req, res) => {
    chemists.find((err, data) => {
        if (err)
            res.send(err);

        res.send({ 'success': true, data });
    });
}

exports.createChemists = (req, res) => {
    chemists.create(req.body.payload, (err, data) => {
        if (err)
            res.send(err);

        res.send({ 'success': true, data });
    });
}

exports.getChemistById = (req, res) => {
    chemists.findOne({ chemistId: req.params.chemistId }, (err, data) => {
        if (err)
            res.send(err);
        res.send({ 'success': true, data });
    });
}