var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var usersSchema = new Schema(
    {
        userId: { type: String, unique: true, required: true },
        name: { type: String, required: true },
        phoneNo: { type: Number, required: true },
        email: { type: String, required: true },
        role: { type: String, required: true }
    }, {
        timestamps: true
    }
);

module.exports = mongoose.model('users', usersSchema);