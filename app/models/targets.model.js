var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var targetsSchema = new Schema(
    {
        userId: { type: String, required: true },
        productId: { type: String, required: true },
        fromDate: { type: Date, required: true },
        endDate: { type: Date, required: true },
        target: { type: Number, required: true },
        type: { type: String, required: true }
    }, {
        timestamps: true
    }
);
module.exports = mongoose.model('targets', targetsSchema);