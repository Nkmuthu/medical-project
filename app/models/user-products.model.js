var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var products = require('./products.model');
var users = require('./users.model');
var targets = require('./targets.model');
var productSales = require('../models/product-sales.model');

var usersProductsSchema = new Schema(
    {
        productId: { type: String, required: true },
        userId: { type: String, required: true },
        product: { type: Schema.Types.ObjectId, ref: 'products' },
        target: { type: Schema.Types.ObjectId, ref: 'targets' },
        sales: [{ type: Schema.Types.ObjectId, ref: 'productSales' }],
        user: { type: Schema.Types.ObjectId, ref: 'users' },
    }, {
        timestamps: true
    }
);

module.exports = mongoose.model('userProducts', usersProductsSchema);