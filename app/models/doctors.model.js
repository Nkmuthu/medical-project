var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var doctorsSchema = new Schema({
    doctorId: { type: String, unique: true, required: true },
    name: { type: String, required: true },
    licenseNo: { type: Number, unique: true, required: true },
    speciality: { type: String, required: true },
    phoneNo: { type: Number, required: true },
    email: { type: String, required: true },
    lastVisit: { type: Date, default: Date.now },
    practice: [{
        name: { type: String, required: true },
        phoneNo: { type: Number, required: true },
        location: { type: String, required: true },
        address: {
            doorNo: { type: String, required: true },
            line1: { type: String, required: true },
            line2: { type: String },
            district: { type: String, required: true },
            state: { type: String, required: true },
            country: { type: String, required: true },
            pincode: { type: Number, required: true }
        }
    }]
});

module.exports = mongoose.model('doctors', doctorsSchema);