var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productsSchema = new Schema(
    {
        productId: { type: String, unique: true, required: true },
        name: { type: String, required: true },
        genericName: { type: String, required: true },
        category: { type: String, required: true },
        unitPrice: { type: Number, required: true }
    }, {
        timestamps: true
    }
);
module.exports = mongoose.model('products', productsSchema);