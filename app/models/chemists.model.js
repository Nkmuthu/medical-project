var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var autoincrement = require('simple-mongoose-autoincrement');
var chemistsSchema = new Schema(
    {
        chemistId: { type: String, unique: true, required: true },
        name: { type: String, required: true },
        ownerName: { type: String, required: true },
        lastVisit: { type: Date, default: Date.now },
        licenseNo: { type: String, required: true },
        phoneNo: { type: Number, required: true },
        emailId: { type: String, required: true },
        location: { type: String, required: true },
        address: {
            doorNo: { type: String, required: true },
            line1: { type: String, required: true },
            line2: { type: String },
            district: { type: String, required: true },
            state: { type: String, required: true },
            country: { type: String, required: true },
            pincode: { type: Number, required: true }
        }
    }, {
        timestamps: true
    }
);
chemistsSchema.plugin(autoincrement, {field: 'sequnece' /*with field name*/});
module.exports = mongoose.model('chemists', chemistsSchema);