var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var doctors = require('./doctors.model');
var chemists = require('./chemists.model');
var productSales = require('../models/product-sales.model');

var productSalesSchema = new Schema(
    {
        chemistId: { type: String, required: true },
        doctorLicenseNo: { type: String, required: true },
        productId: { type: String, required: true },
        quantity: { type: Number, required: true },
        dateOfSold: { type: Date, required: true },
        userId: { type: String, required: true },
        chemist: { type: Schema.Types.ObjectId, ref: 'chemists' },
        doctor: { type: Schema.Types.ObjectId, ref: 'doctors' },
    }, {
        timestamps: true
    }
);
module.exports = mongoose.model('productSales', productSalesSchema);