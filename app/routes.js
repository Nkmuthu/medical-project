var API_VERSION = `/api/v1`;
var usersController = require('./controllers/users.controller');
var chemistsController = require('./controllers/chemists.controller');
var doctorsController = require('./controllers/doctors.controller');
var productsController = require('./controllers/products.controller');
var userProductsController = require('./controllers/user-products.controller');
var productSalesController = require('./controllers/product-sales.controller');
var targetsController = require('./controllers/targets.controller');
var mrDashboardController = require('./controllers/mr-dashboard.controller');

module.exports = (app) => {
    app.route(`${API_VERSION}/users`)
        .get(usersController.getAllUsers)
        .post(usersController.createUsers);

    app.route(`${API_VERSION}/users/:userId`)
        .get(usersController.getUserById);

    app.route(`${API_VERSION}/chemists`)
        .get(chemistsController.getAllChemists)
        .post(chemistsController.createChemists);

    app.route(`${API_VERSION}/chemists/:chemistId`)
        .get(chemistsController.getChemistById);

    app.route(`${API_VERSION}/doctors`)
        .get(doctorsController.getAllDoctors)
        .post(doctorsController.createDoctors);

    app.route(`${API_VERSION}/doctors/:doctorId`)
        .get(doctorsController.getDoctorById);

    app.route(`${API_VERSION}/products`)
        .get(productsController.getAllProducts)
        .post(productsController.createProducts);

    app.route(`${API_VERSION}/user/products`)
        .get(userProductsController.getAllUserProducts)
        .post(userProductsController.createUserProducts);

    app.route(`${API_VERSION}/user/products/:userId`)
        .get(userProductsController.getUserProductById);

    app.route(`${API_VERSION}/product-sales`)
        .post(productSalesController.createProductSales);

    app.route(`${API_VERSION}/targets`)
        .get(targetsController.getAllTargets)
        .post(targetsController.createTargets);

    app.route(`${API_VERSION}/targets/:userId`)
        .get(targetsController.getTargetByUserId);

    app.route(`${API_VERSION}/product-target-sales/:userId/:type/:start/:end`)
        .post(mrDashboardController.getProductTargetSales);

    app.route(`${API_VERSION}/single-product-target-sales/:type/:userId/:productId`)
        .post(mrDashboardController.getSingleProductTargetSales);

    app.route(`${API_VERSION}/product-sales-by-doctor/:userId/:productId/:startDate/:endDate`)
        .get(mrDashboardController.getProductSalesByDoctor);

    app.route(`${API_VERSION}/product-sales-by-chemist/:userId/:productId/:startDate/:endDate`)
        .get(mrDashboardController.getProductSalesByChemist);
};